//
//  Constants.swift
//  Weather
//
//  Created by Anupriya Soman on 11/05/2021.
//

import Foundation
import UIKit

class Constants {
    
    struct Config {
        static let APIKey = "73455cf6a887a2f84d9d4b65f9581207"
        static let ForecastURL = "https://api.openweathermap.org/data/2.5/forecast?"
        static let WeatherURL = "https://api.openweathermap.org/data/2.5/weather?"
    }
    
    struct Defaults {
        static let AppBlueColor = UIColor(red: 81/255, green: 139/255, blue: 190/255, alpha: 1.0)
        static let AppYellowColor = UIColor(red: 240/255, green: 215/255, blue: 77/255, alpha: 1.0)
    }
    
    enum CellIdentifiers {
        static let dayTableViewCell          = "DayTableViewCell"
        static let weatherCollectionViewCell = "WeatherCollectionViewCell"
        static let cityCollectionViewCell = "CityCollectionViewCell"
    }
    
    enum NibFile {
        static let dayTableViewCell          = "DayTableViewCell"
        static let weatherCollectionViewCell = "WeatherCollectionViewCell"
        static let cityCollectionViewCell = "CityCollectionViewCell"
    }
}
