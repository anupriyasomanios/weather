//
//  ForecastViewModel.swift
//  Weather
//
//  Created by Anupriya Soman on 11/05/2021.
//

import Foundation

public class ForecastViewModel: ObservableObject {
    @Published var cityName: String = "City Name"
    var modelCount = 0
    var viewModel = [DayViewModelMapper]()
   
    public let weatherService: WeatherService
    public init(weatherService: WeatherService) {
        self.weatherService = weatherService
    }
    
    public func refresh() {
      
        weatherService.loadWeatherData { weather in
            DispatchQueue.main.async { [self] in
                self.cityName = weather.city
                let model = self.parseWeatherData(data: weather)
                self.modelCount = model!.count
                self.viewModel = model!
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "updateForecastData"), object: nil, userInfo: nil)
            }
        }
    }
    
   func parseWeatherData(data: Forecast) ->  [DayViewModelMapper]? {

        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .long
        dateFormatter.timeStyle = .none
        var weatherCondition:String = ""
        var dayModels: [DayModel] = []
        var dayModel = DayModel(forecastDate: "", forecastTime: "", weatherList:[])
        var dayDate:String = ""
        
        for i in (0..<data.arrayData.count) {
           
            let realValue = data.arrayData[i]as! APIList
            let temp = realValue.main.temp
            let weatherArray = realValue.weather
            for item in weatherArray {
                let realitem = item
                weatherCondition = realitem.iconName
            }
            let time = realValue.dt_txt
            let timeComponents = time.components(separatedBy: " ")
            
            if dayDate == "" || dayDate == timeComponents[0] {
                
                dayDate = timeComponents[0]
                if dayModel.forecastDate == "", dayModel.forecastTime == ""{
                    dayModel.forecastDate = timeComponents[0]
                    dayModel.forecastTime = timeComponents[1]
                }
                
                let weatherModel = WeatherModel.init(temperature: temp, weatherCondition: weatherCondition, weatherImage: weatherCondition)
                dayModel.weatherList.append(weatherModel)
                
            }else {
            
                dayModels.append(dayModel)
                dayModel = DayModel(forecastDate: "", forecastTime: "", weatherList:[])
                dayModel.forecastDate = timeComponents[0]
                dayModel.forecastTime = timeComponents[1]
                let weatherModel = WeatherModel.init(temperature: temp, weatherCondition: weatherCondition, weatherImage: weatherCondition)
                dayModel.weatherList.append(weatherModel)
                dayDate = dayModel.forecastDate
            }
        }
        var dayViewModelList:[DayViewModelMapper] = []
        
        for tempDayModel in dayModels {
            dayViewModelList.append(DayViewModelMapper(dayModel: tempDayModel))
        }
        return dayViewModelList
    }
}







