//
//  DayViewModel.swift
//  Weather
//
//  Created by Anupriya Soman on 11/05/2021.
//

import UIKit

protocol DayViewModel {
    var dayModel: DayModel { get }
    var day: String { get }
    var weatherForecast: [WeatherViewModel] { get }
}

class DayViewModelMapper: NSObject, DayViewModel {
    
    var dayModel: DayModel
    var day: String
    var weatherForecast: [WeatherViewModel]
    
    init(dayModel: DayModel) {
        self.dayModel = dayModel
        self.day = ""
        self.weatherForecast = []
        super.init()
        self.setupViewData(dayModel: dayModel)
    }
    
    func setupViewData(dayModel: DayModel) {
        self.day = self.getDateFormat(dateString: dayModel.forecastDate)
        for weatherModel in self.dayModel.weatherList {
            self.weatherForecast.append(WeatherViewModelMapper(weather: weatherModel))
        }
    }
}

extension DayViewModelMapper {
    
    func getDateFormat(dateString: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        dateFormatter.timeZone = NSTimeZone(name: "UTC") as TimeZone?
        let date = dateFormatter.date(from: dateString)
        dateFormatter.dateFormat = "dd MMM YYYY"
        dateFormatter.timeZone = NSTimeZone.local
        let timeStamp = dateFormatter.string(from: date!)
        return timeStamp
    }
    
    func getDayOfWeek(_ today:String) -> String {
        
        let formatter  = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        guard let todayDate = formatter.date(from: today) else { return "" }
        let myCalendar = Calendar(identifier: .gregorian)
        return formatter.weekdaySymbols[myCalendar.component(.weekday, from: todayDate)]
    }
}

