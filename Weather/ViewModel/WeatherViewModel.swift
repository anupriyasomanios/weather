//
//  WeatherViewModel.swift
//  Weather
//
//  Created by Anupriya Soman on 11/05/2021.
//

import UIKit

protocol WeatherViewModel {
    var temperature: String { get }
    var weatherCondition: String{ get }
    var weatherImage: String{ get }
}

private let defaultIcon = "❓"

private let iconMap = ["Drizzle" : "🌧",
                       "Thunderstorm": "⛈",
                       "Rain": "🌧",
                       "Snow": "❄️",
                       "Clear": "☀️",
                       "Clouds": "💨",]

class WeatherViewModelMapper: NSObject, WeatherViewModel {
   
    var temperature: String
    var weatherCondition: String
    var weatherImage: String

    init(weather: WeatherModel) {
        temperature =  String(format: "%0.f °C", weather.temperature)
        weatherCondition = weather.weatherCondition
        weatherImage = iconMap[weather.weatherImage] ?? defaultIcon
        super.init()
    }
}



