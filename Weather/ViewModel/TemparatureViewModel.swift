//
//  TemparatureViewModel.swift
//  Weather
//
//  Created by Anupriya Soman on 12/05/2021.
//

import Foundation

struct TemparatureViewModel {
    let response: TemparatureModel
}

extension TemparatureViewModel {
    init(_ response: TemparatureModel) {
        self.response = response
    }
}

extension TemparatureViewModel {
    var description: String {
        return self.response.description
    }
    
    var tempImage: String {
        return self.response.tempImage
    }
    
    var temp_max: Double {
        return self.response.maxTemparature
    }
    
    var temp_min: Double {
        return self.response.minTemparature
    }
    
    var speed: Double {
        return self.response.speed
    }
}
