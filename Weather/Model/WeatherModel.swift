//
//  WeatherModel.swift
//  Weather
//
//  Created by Anupriya Soman on 11/05/2021.
//

import Foundation

class WeatherModel: NSObject {
    
    var temperature: Double
    var weatherCondition: String
    var weatherImage: String
    
    init(temperature: Double, weatherCondition: String, weatherImage: String) {
        self.temperature      = temperature
        self.weatherCondition = weatherCondition
        self.weatherImage = weatherImage
    }
}
