//
//  TemparatureModel.swift
//  Weather
//
//  Created by Anupriya Soman on 11/05/2021.
//

import Foundation

public struct TemparatureModel {
   
    let tempImage: String
    let description: String
    let speed: Double
    let maxTemparature: Double
    let minTemparature: Double
    
    init(response: APITemparature) {
        
        description = response.weather.first!.description
        tempImage = response.weather.first!.tempImage
        speed = response.wind.speed
        maxTemparature = response.main.temp_max
        minTemparature = response.main.temp_min
    }
}

struct APITemparature: Decodable {
    let weather: [APITempWeather]
    let wind: APITempWind
    let main: APITempMain
}


struct APITempWeather: Decodable {
    let description: String
    let tempImage: String
    
    enum CodingKeys: String, CodingKey {
        case description
        case tempImage = "main"
    }
}

struct APITempMain: Decodable {
    let temp_max: Double
    let temp_min: Double
    
}

struct APITempWind: Decodable {
    let speed: Double
}


