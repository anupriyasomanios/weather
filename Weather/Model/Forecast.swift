//
//  Forecast.swift
//  Weather
//
//  Created by Anupriya Soman on 11/05/2021.
//

import Foundation

public struct Forecast {
    let city: String
    let description: String
    let iconName: String
    let dateText: String
    let arrayData: Array<Any>
    let temparature: Double
    
    init(response: APIResponse) {
        arrayData = response.list
        city = response.city.name
        description = response.list.first?.weather.description ?? ""
        iconName = response.list.first?.weather.description ?? ""
        dateText = response.list.first?.dt_txt ?? ""
        temparature = response.list.first?.main.temp ?? 0
    }
}

struct APIResponse: Decodable {
    let city: APICity
    let list: [APIList]
}

struct APIList: Decodable {
    let weather: [APIWeather]
    let dt_txt: String
    let main: APIMain
}

struct APIMain: Decodable {
    let temp: Double
}

struct APIWeather: Decodable {
    let description: String
    let iconName: String
    
    enum CodingKeys: String, CodingKey {
        case description
        case iconName = "main"
    }
}

struct APICity: Decodable {
    let name: String
}
