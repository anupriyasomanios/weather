//
//  WeatherController.swift
//  Weather
//
//  Created by Anupriya Soman on 11/05/2021.
//

import UIKit
import RealmSwift

class WeatherController: UIViewController, UISearchBarDelegate {
    
    @IBOutlet weak var forecastTableView: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var cityNameLabel: UILabel!
    private var forecastViewModel: ForecastViewModel!
    lazy var realm = try! Realm()
    

    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(loadForecastData), name: NSNotification.Name(rawValue: "updateForecastData"), object: nil)
        setUp()
    }
    
    @objc func loadForecastData() {
        updateUI()
    }

    private func setUp() {
        forecastTableView.rowHeight = UITableView.automaticDimension
        forecastTableView.register(UINib.init(nibName: Constants.NibFile.dayTableViewCell, bundle: nil), forCellReuseIdentifier: Constants.CellIdentifiers.dayTableViewCell)
        forecastViewModel = ForecastViewModel(weatherService: WeatherService())
        forecastViewModel.refresh()
    }
    
    func updateUI() {
        cityNameLabel.text = self.forecastViewModel.cityName
        self.write()
        self.forecastTableView.reloadData()
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
    
        searchBar.resignFirstResponder()
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewController = storyBoard.instantiateViewController(withIdentifier: "SearchController") as! SearchController
        self.navigationController?.pushViewController(newViewController, animated: true)
    }
}
extension WeatherController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let numberOfRows = forecastViewModel?.modelCount else { return 0   }
        return numberOfRows
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let cell = forecastTableView.dequeueReusableCell(withIdentifier: Constants.CellIdentifiers.dayTableViewCell) as? DayTableViewCell {
            cell.viewModel = forecastViewModel?.viewModel[indexPath.row]
            cell.weatherCollectionView.reloadData()
            cell.weatherCollectionView.collectionViewLayout.invalidateLayout()
            return cell
        }
        return UITableViewCell()
    }
}

// MARK: RealM DB Methods
extension WeatherController {
    private func write() {
        let existingCity = realm.object(ofType: City.self, forPrimaryKey: self.forecastViewModel.cityName)
        if existingCity != nil {
        } else {
            if realm.objects(City.self).count == 5 {
                self.delete()
            }
            let city = City.create(withName: self.forecastViewModel.cityName, id: self.forecastViewModel.cityName)
            try! realm.write {
                realm.add(city)
            }
        }
    }
    
    private func delete() {
        if let itemToDelete = realm.objects(City.self).last {
            try! realm.write {
                realm.delete(itemToDelete)
            }
        }
    }
}

class City: Object {
    @objc dynamic var id = ""
    @objc dynamic var name = ""
    
    override class func primaryKey() -> String {
        return "id"
    }
    
    static func create(withName name: String, id: String) -> City {
        let city = City()
        city.name = name
        city.id = id
        return city
    }
}
