//
//  SearchController.swift
//  Weather
//
//  Created by Anupriya Soman on 11/05/2021.
//

import UIKit
import RealmSwift

private let defaultIcon = "❓"

private let iconMap = ["Drizzle" : "🌧",
                       "Thunderstorm": "⛈",
                       "Rain": "🌧",
                       "Snow": "❄️",
                       "Clear": "☀️",
                       "Clouds": "💨",
                       "Haze": "☁️"]
class SearchController: UIViewController {
    
    var tempViewModel: TemparatureViewModel!
    let realm = try! Realm()
    
    @IBOutlet weak var noRecentSearchLabel: UILabel!
    @IBOutlet weak var cityNameLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var weatherImage: UILabel!
    @IBOutlet weak var windSpeedLabel: UILabel!
    @IBOutlet weak var minTemparatureLabel: UILabel!
    @IBOutlet weak var maxTemparatureLabel: UILabel!
    @IBOutlet weak var cityCollectionview: UICollectionView!
   
    var cityInfo: Results<City> {
        get {
            return realm.objects(City.self)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        cityCollectionview.register(UINib(nibName: Constants.NibFile.cityCollectionViewCell, bundle: nil), forCellWithReuseIdentifier: Constants.CellIdentifiers.cityCollectionViewCell)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        noRecentSearchLabel.isHidden = true
        if cityInfo.count == 0 {
            noRecentSearchLabel.isHidden = false
            noRecentSearchLabel.text = "No recent search found!"
        }
    }
    
    func setUp(selectedCity: String) {
        WeatherService().getSearchResults(city: selectedCity) { data in
            if let tempDetails = data {
                self.tempViewModel = TemparatureViewModel(response: tempDetails)
                DispatchQueue.main.async {
                    self.cityNameLabel.text = selectedCity
                    self.updateUI()
                }
            }
        }
    }
    
    func updateUI() {
       
        self.descriptionLabel.text = self.tempViewModel.description.capitalized
        self.weatherImage.text = iconMap[self.tempViewModel.tempImage] ?? defaultIcon
        self.windSpeedLabel.text = String(format: "Wind Speed: %.0f mps", self.tempViewModel.speed)
        self.maxTemparatureLabel.text = String(format: "Max: %.0f °C", self.tempViewModel.temp_max)
        self.minTemparatureLabel.text = String(format: "Min: %.0f °C", self.tempViewModel.temp_min)
    }
    
    @IBAction func backClickAction(sender: UIButton) {
        _ = self.navigationController?.popToRootViewController(animated: true)
    }
}

extension SearchController: UICollectionViewDataSource, UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return cityInfo.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = cityCollectionview.dequeueReusableCell(withReuseIdentifier: Constants.CellIdentifiers.cityCollectionViewCell, for: indexPath) as? CityCollectionViewCell {
            cell.cityNameLabel.text = cityInfo[indexPath.row].name
            cell.contentView.backgroundColor = Constants.Defaults.AppYellowColor
            cell.contentView.layer.cornerRadius = 15.0
            return cell
        }
        return UICollectionViewCell()
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let city = cityInfo[indexPath.row].name
        self.setUp(selectedCity: city)
    }
    
}

extension SearchController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.size.width/3, height: 150)
    }
}



