//
//  WeatherService.swift
//  Weather
//
//  Created by Anupriya Soman on 11/05/2021.
//

import Foundation
import CoreLocation

public final class WeatherService: NSObject {
    private let locationManager = CLLocationManager()
    private let API_KEY = Constants.Config.APIKey
    private var completionHandler: ((Forecast) -> Void)?
    private var newcompletionHandler: ((TemparatureModel) -> Void)?
    
    public override init() {
        super.init()
        locationManager.delegate = self
    }
    
    public func loadWeatherData(_ completionHandler: @escaping((Forecast) -> Void)) {
        self.completionHandler = completionHandler
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
    }
    
    private func makeForecastDataRequest(forCoordinates coordinates: CLLocationCoordinate2D) {
        let baseUrlString = String(format: "%@lat=\(coordinates.latitude)&lon=\(coordinates.longitude)&appid=\(API_KEY)&units=metric", Constants.Config.ForecastURL)
        guard let urlString = baseUrlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) else { return }
        guard let url = URL(string: urlString) else { return }
        URLSession.shared.dataTask(with: url) { data, response, error in
            guard error == nil, let data = data else { return }
            if let response = try? JSONDecoder().decode(APIResponse.self, from: data) {
                self.completionHandler?(Forecast(response: response))
            }
        }.resume()
    }
    
    
    public  func getSearchResults(city: String,completion: @escaping (TemparatureModel?) -> ()) {
        let baseUrlString = String(format: "%@q=\(city)&appid=\(API_KEY)&units=metric", Constants.Config.WeatherURL)
        guard let urlString = baseUrlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) else { return }
        guard let url = URL(string: urlString) else { return }
        URLSession.shared.dataTask(with: url) { data, response, error in
            guard error == nil, let data = data else { return }
            if let response = try? JSONDecoder().decode(APITemparature.self, from: data) {
                completion(TemparatureModel(response: response))
            }
        }.resume()
      }
}

extension WeatherService: CLLocationManagerDelegate {
    public func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let location = locations.first else { return }
        makeForecastDataRequest(forCoordinates: location.coordinate)
    }
    
    public func locationManager(_ manager: CLLocationManager, didFailWithError error: Error)
    {
        print("Something went wrong\(error.localizedDescription)")
    }
}




