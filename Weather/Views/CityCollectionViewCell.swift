//
//  CityCollectionViewCell.swift
//  Weather
//
//  Created by Anupriya Soman on 12/05/2021.
//

import UIKit

class CityCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var cityNameLabel: UILabel!

}
