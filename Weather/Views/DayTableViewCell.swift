//
//  DayTableViewCell.swift
//  Weather
//
//  Created by Anupriya Soman on 11/05/2021.

import UIKit

class DayTableViewCell: UITableViewCell {
    
    @IBOutlet weak var dayLabel: UILabel!
    @IBOutlet weak var weatherCollectionView: UICollectionView!
    
    let cellWidth = 150
    let cellHeight = 150
    
    
    var viewModel: DayViewModel? {
        didSet {
            self.dayLabel.text = viewModel?.day
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupView()
    }
    
    func setupView() {
        weatherCollectionView.register(UINib(nibName: Constants.NibFile.weatherCollectionViewCell, bundle: nil), forCellWithReuseIdentifier: Constants.CellIdentifiers.weatherCollectionViewCell)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}

extension DayTableViewCell: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        guard let items = viewModel?.weatherForecast.count else { return 0 }
        return items
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = weatherCollectionView.dequeueReusableCell(withReuseIdentifier: Constants.CellIdentifiers.weatherCollectionViewCell, for: indexPath) as? WeatherCollectionViewCell {
            cell.viewModel = viewModel?.weatherForecast[indexPath.row]
            return cell
        }
        return UICollectionViewCell()
    }
}

extension DayTableViewCell: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: cellWidth, height: cellHeight)
    }
}
