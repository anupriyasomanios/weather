//
//  WeatherTableViewCell.swift
//  WeatherTron
//
//  Created by Anupriya Soman on 11/05/2021.

import UIKit

class WeatherCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var weatherConditionLabel: UILabel!
    @IBOutlet weak var temperatureLabel: UILabel!
    @IBOutlet weak var weatherImageLabel: UILabel!
    
    var viewModel: WeatherViewModel? {
        didSet {
           self.weatherConditionLabel.text = viewModel?.weatherCondition
           self.temperatureLabel.text = viewModel?.temperature
            self.weatherImageLabel.text = viewModel?.weatherImage
        }
    }
}
