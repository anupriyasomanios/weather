#  Weather - iOS App

## Install ##
* $ sudo gem install cocoapods
* $ pod install
---

* pod 'RealmSwift'


Weather is a forecast ios app which displays 5 day forecast at any location or city. It includes weather forecast data with 3-hour step
where data collected from https://openweathermap.org/.
RealMDB used to store recent search keyword and it displays weather information for selected city.

Requirements

+ Swift 5
+ Xcode 12


